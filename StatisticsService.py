import calendar 
import time
import datetime

#    StatsBot
#    Copyright (C) 2019  Felix v. O.
#    felix@von-oertzen-berlin.de
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

class StatisticsService:
  def __init__(self, db):
    self.__db = db
  
  def loadStatisticsFor(self, userID, timeDelta):
    userTime = [['online', 0],['idle', 0], ['dnd', 0]]
    lastEvent = ""
    lastTime = 0
    
    for entry in self.__db.getTimeLine(userID, timeDelta) + [("offline", str(time.strftime("%Y-%m-%d %T", time.gmtime())))]: 
      t = calendar.timegm(time.strptime(entry[1], "%Y-%m-%d %H:%M:%S"))
      e = str(entry[0])
      if lastTime != 0: 
        duration  = t - lastTime
        for ut in userTime:
          if ut[0] == lastEvent:
            ut[1] = ut[1] + duration
      lastTime  = t
      lastEvent = e 
    return (userTime)
